'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _passportOauth = require('passport-oauth');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * `Strategy` constructor.
 * The Vkontakte authentication strategy authenticates requests by delegating to Vkontakte using OAuth2 access tokens.
 * Applications must supply a `verify` callback which accepts a accessToken, refreshToken, profile and callback.
 * Callback supplying a `user`, which should be set to `false` if the credentials are not valid.
 * If an exception occurs, `error` should be set.
 *
 * Options:
 * - clientID          Identifies client to Vkontakte App
 * - clientSecret      Secret used to establish ownership of the consumer key
 * - passReqToCallback If need, pass req to verify callback
 *
 * @param {Object} _options
 * @param {Function} _verify
 * @example
 * passport.use(new VKontakteTokenStrategy({
 *   clientID: '123456789',
 *   clientSecret: 'shhh-its-a-secret'
 * }), function(accessToken, refreshToken, profile, next) {
 *   User.findOrCreate({vkontakteId: profile.id}, function(error, user) {
 *     next(error, user);
 *   })
 * })
 */

var VKontakteTokenStrategy = function (_OAuth2Strategy) {
  _inherits(VKontakteTokenStrategy, _OAuth2Strategy);

  function VKontakteTokenStrategy(_options, _verify) {
    _classCallCheck(this, VKontakteTokenStrategy);

    var options = _options || {};
    var verify = _verify;

    options.authorizationURL = options.authorizationURL || 'https://oauth.vk.com/authorize';
    options.tokenURL = options.tokenURL || 'https://oauth.vk.com/access_token';

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(VKontakteTokenStrategy).call(this, options, verify));

    _this.name = 'vkontakte-token';
    _this._accessTokenField = options.accessTokenField || 'access_token';
    _this._refreshTokenField = options.refreshTokenField || 'refresh_token';
    _this.email = options.email || 'email';
    _this._profileFields = options.profileFields || ['uid', 'first_name', 'last_name', 'screen_name', 'sex', 'photo'];
    _this._profileURL = options.profileURL || 'https://api.vk.com/method/users.get';
    _this._apiVersion = options.apiVersion || '5.0';
    _this._state = options.state || [];
    _this._passReqToCallback = options.passReqToCallback;
    return _this;
  }

  /**
   * Authenticate method
   * @param {Object} req
   * @param {Object} options
   * @returns {*}
   */

  _createClass(VKontakteTokenStrategy, [{
    key: 'authenticate',
    value: function authenticate(req, options) {
      var _this2 = this;

      var accessToken = req.body && req.body[this._accessTokenField] || req.query && req.query[this._accessTokenField];
      var refreshToken = req.body && req.body[this._refreshTokenField] || req.query && req.query[this._refreshTokenField];
      var email = req.body && req.body[this.email] || req.query && req.query[this.email];
      var _state = {};
      this._state.forEach(function (item) {
            _state[item] = req.body && req.body[item] || req.query && req.query[item];
      });
      this._state = _state;
      if (!accessToken) return this.fail({ message: 'You should provide ' + this._accessTokenField });

      this._loadUserProfile(accessToken, function (error, profile) {
        if (error) return _this2.error(error);

        var verified = function verified(error, user, info) {
          if (error) return _this2.error(error);
          if (!user) return _this2.fail(info);

          return _this2.success(user, info);
        };

        if (_this2._passReqToCallback) {
          _this2._verify(req, accessToken, refreshToken, email, profile, verified);
        } else {
          _this2._verify(accessToken, refreshToken, email, profile, verified);
        }
      });
    }

    /**
     * Parse user profile
     * @param {String} accessToken Vkontakte OAuth2 access token
     * @param {Function} done
     */

  }, {
    key: 'userProfile',
    value: function userProfile(accessToken, done) {
      var url = this._profileURL + '?fields=' + this._profileFields.join(',') + '&v=' + this._apiVersion;
      var state = this._state;
      this._oauth2.get(url, accessToken, function (error, body, res) {
        if (error) return done(new _passportOauth.InternalOAuthError('Failed to fetch user profile', error));

        try {
          var json = JSON.parse(body);
          if (json.error) return done(new _passportOauth.InternalOAuthError(json.error.error_msg, json.error.error_code));

          json = json.response[0];

          var profile = {
            provider: 'vkontakte',
            id: json.id,
            state: state,
            username: json.screen_name,
            displayName: json.first_name + ' ' + json.last_name,
            name: {
              familyName: json.last_name || '',
              givenName: json.first_name || ''
            },
            emails: [],
            photos: [],
            _raw: body,
            _json: json
          };

          return done(null, profile);
        } catch (e) {
          return done(e);
        }
      });
    }
  }]);

  return VKontakteTokenStrategy;
}(_passportOauth.OAuth2Strategy);

exports.default = VKontakteTokenStrategy;
module.exports = exports['default'];